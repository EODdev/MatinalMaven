package fr.isika.al13.force_java_docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForceJavaDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForceJavaDockerApplication.class, args);
	}

}
