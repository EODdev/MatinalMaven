package fr.isika.al13.force_java_docker.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class PersonneController {
	
	Logger log = LoggerFactory.getLogger(this.getClass());  

	@GetMapping("/index")        
	public String accueil(){
		log.info("---------------------------- Envoi requête vers Accueil"); 


		return "Accueil";    
	}
	
	
	
	@PostMapping(value = "/saisir-nom")
	public String saisirNom(Model model, @RequestParam(name = "nom",required=false) String nom, @RequestParam(name = "genre",required=false) String genre){
		String phrase = "";
		if(genre!=null) {
			if(genre.equals("f")) {
				phrase = "Bonjour " + nom + " vous êtes bien matinale.";
				
			} else {
				phrase = "Bonjour " + nom + " vous êtes bien matinal.";
			}
		} else {
			phrase = "Bonjour " + nom + " vous êtes bien matinal/e.";
		}
		
		model.addAttribute("nom", phrase);

		return "affichage";
	}

}
